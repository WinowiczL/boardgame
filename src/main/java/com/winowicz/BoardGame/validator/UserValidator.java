package com.winowicz.BoardGame.validator;

import java.util.List;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.bo.Criteria;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.exceptions.BadValueOfInputException;
import com.winowicz.BoardGame.exceptions.UserDoesntExistException;
import com.winowicz.BoardGame.exceptions.UserExistException;
import com.winowicz.BoardGame.mapper.dto.UserDto;

@Component
public class UserValidator {

	public boolean validate(String newInformation) {
		if (newInformation == "") {
			throw new BadValueOfInputException();
		}
		return true;
	}

	public boolean isUserExist(Criteria criteria) {
		if (criteria.getFirstName() == null && criteria.getLastName() == null && criteria.getEmail() == null) {
			throw new UserDoesntExistException();
		}
		if (criteria.getFirstName() == "" && criteria.getLastName() == "" && criteria.getEmail() == null) {
			throw new UserDoesntExistException();
		}
		return true;
	}

	public boolean isUserExist(String firstName, String lastName, String email) {
		if (firstName == null && lastName == null && email == null) {
			throw new UserDoesntExistException();
		}

		if (firstName == "" && lastName == "" && email == "") {
			throw new UserDoesntExistException();
		}
		return true;
	}

	public boolean isAnyUser(List<UserDto> list) {
		if (list.isEmpty()) {
			throw new UserDoesntExistException();
		}
		return true;
	}

	public boolean isUserEmailExist(String email, List<User> list) {
		if (list.stream().anyMatch(u -> u.getEmail().equals(email))) {
			throw new UserExistException();
		}
		return false;
	}

}
