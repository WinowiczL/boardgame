package com.winowicz.BoardGame.validator;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.bo.AvailabilityPeriod;
import com.winowicz.BoardGame.exceptions.BadDataTimeException;

@Component
public class PlayabilityValidator {

	public boolean isAddingFutureAvailability(AvailabilityPeriod availabilityHours) {

		if (availabilityHours.getStart().isBefore(LocalDateTime.now())) {
			throw new BadDataTimeException("You can't add past availabilty time");
		}
		return true;

	}

	public boolean isStartTimeBeforeEnd(AvailabilityPeriod availabilityHours) {

		if (availabilityHours.getStart().isAfter(availabilityHours.getEnd())) {
			throw new BadDataTimeException("End of availability hours cant be before start");
		}
		return true;
	}

	public boolean isAvailabilityHoursInPlayable(AvailabilityPeriod availabilityHours) {
		if (availabilityHours.getStatus() == false) {
			throw new BadDataTimeException("You dont have this availability hours");
		}
		return true;
	}

}
