package com.winowicz.BoardGame.validator;

import java.util.List;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.exceptions.GameDoesntExistException;
import com.winowicz.BoardGame.exceptions.GameExistException;

@Component
public class BoardGameValidator {

	public boolean isGameInSystemExist(String nameOfGame, List<String> systemGamesList) {
		if (isOnList(nameOfGame, systemGamesList)) {
			return true;
		} else {
			throw new GameDoesntExistException("in system");
		}

	}

	public boolean isGameInCollectionExist(String nameOfGame, List<String> systemGamesList) {
		if (isOnList(nameOfGame, systemGamesList)) {
			return true;
		} else {
			throw new GameDoesntExistException("in collection");
		}
	}

	public boolean isDuplicateInSystem(String nameOfGame, List<String> systemGamesList) {
		if (isOnList(nameOfGame, systemGamesList)) {
			throw new GameExistException("in system");
		}
		return false;
	}

	public boolean isDuplicateInCollection(String nameOfGame, List<String> userGameList) {
		if (isOnList(nameOfGame, userGameList)) {
			throw new GameExistException("in collection");
		}
		return false;
	}

	private boolean isOnList(String nameOfGame, List<String> gameList) {
		return gameList.stream().anyMatch(u -> u.equals(nameOfGame));
	}

}
