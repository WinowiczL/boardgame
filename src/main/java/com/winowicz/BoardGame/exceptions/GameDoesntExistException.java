package com.winowicz.BoardGame.exceptions;

public class GameDoesntExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1375649617679996178L;

	public GameDoesntExistException() {
		super("Game doesnt exist");
	}

	public GameDoesntExistException(String message) {
		super("Game doesnt exist " + message);
	}
}
