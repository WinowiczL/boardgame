package com.winowicz.BoardGame.exceptions;

public class BadDataTimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1384567577858233148L;

	public BadDataTimeException() {
		super("Wrong data or time");
	}

	public BadDataTimeException(String message) {
		super("Wrong data or time, " + message);
	}

}
