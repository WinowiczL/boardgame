package com.winowicz.BoardGame.exceptions;

public class UserDoesntExistException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4367389182150350600L;

	public UserDoesntExistException() {
		super("User Doesnt Exist");
	}
	
	public UserDoesntExistException(String message) {
		super("User Doesnt Exist " + message);
	}

}
