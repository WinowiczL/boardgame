package com.winowicz.BoardGame.exceptions;

public class BadValueOfInputException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4985529787818706161L;

	public BadValueOfInputException() {
		super("Don't give empty field");
	}

	public BadValueOfInputException(String message) {
		super("Don't give empty field " + message);
	}
}
