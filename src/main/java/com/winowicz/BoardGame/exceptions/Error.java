package com.winowicz.BoardGame.exceptions;

import java.util.Date;

public class Error {
	
	private Date date;
	private String fail;
	
	public Error(String error) {
		this.date = new Date();
		this.fail = error;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getError() {
		return fail;
	}

	public void setError(String error) {
		this.fail = error;
	}
	
	

}
