package com.winowicz.BoardGame.exceptions;

public class GameExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GameExistException() {
		super("Game exist");
	}

	public GameExistException(String message) {
		super("Game exist" + message);
	}
}
