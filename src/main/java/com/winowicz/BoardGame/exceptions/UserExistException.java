package com.winowicz.BoardGame.exceptions;

public class UserExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6776607217105757975L;

	public UserExistException() {
		super("User With This Email Exist");
	}

	public UserExistException(String message) {
		super("User With This Email Exist " + message);
	}
}
