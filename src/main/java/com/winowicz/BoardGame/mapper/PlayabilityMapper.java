package com.winowicz.BoardGame.mapper;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.bo.Playability;
import com.winowicz.BoardGame.mapper.dto.PlayabilityDto;

@Component
public class PlayabilityMapper {

	public Optional<PlayabilityDto> map(Optional<Playability> playability) {
		PlayabilityDto playabilityDto = new PlayabilityDto();
		if (playability.isPresent()) {
			playabilityDto.setAvailabilityHours(playability.get().getAvailabilityHours());
			playabilityDto.setId(playability.get().getId());
		}

		return Optional.ofNullable(playabilityDto);
	}
}
