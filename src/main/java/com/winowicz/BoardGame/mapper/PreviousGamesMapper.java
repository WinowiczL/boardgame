package com.winowicz.BoardGame.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.bo.PreviousGames;
import com.winowicz.BoardGame.mapper.dto.PreviousGamesDto;

@Component
public class PreviousGamesMapper {

	public List<PreviousGamesDto> map(List<PreviousGames> previousGames) {
		List<PreviousGamesDto> list = new ArrayList<>();

		for (PreviousGames element : previousGames) {
			list.add(new PreviousGamesDto(element.getPreviousGame(), element.getBoardGameName()));
		}
		return list;
	}

}
