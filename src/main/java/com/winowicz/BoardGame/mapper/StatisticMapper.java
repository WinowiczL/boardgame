package com.winowicz.BoardGame.mapper;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.bo.Statistic;
import com.winowicz.BoardGame.mapper.dto.StatisticDto;

@Component
public class StatisticMapper {

	public Optional<StatisticDto> map(Optional<Statistic> statistic) {
		StatisticDto statisticDto = new StatisticDto();
		if (statistic.isPresent()) {
			statisticDto.setId(statistic.get().getId());
			statisticDto.setWin(statistic.get().getWin());
			statisticDto.setLoss(statistic.get().getLoss());
			statisticDto.setDraw(statistic.get().getDraw());
			statisticDto.setRankingPosition(statistic.get().getRankingPosition());
			statisticDto.setLevel(statistic.get().getLevel());
		}
		return Optional.ofNullable(statisticDto);
	}

}
