package com.winowicz.BoardGame.mapper;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.bo.BoardGame;
import com.winowicz.BoardGame.mapper.dto.BoardGameDto;

@Component
public class BoardGameMapper {

	public Optional<BoardGameDto> map(Optional<BoardGame> boardGame) {
		BoardGameDto boardGameDto = new BoardGameDto();
		if (boardGame.isPresent()) {
			boardGameDto.setBoardGamesNames(boardGame.get().getBoardGamesNames());
		}
		return Optional.ofNullable(boardGameDto);
	}
}
