package com.winowicz.BoardGame.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.winowicz.BoardGame.bo.PreviousGames;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.mapper.dto.PreviousGamesDto;
import com.winowicz.BoardGame.mapper.dto.UserDto;

@Component
public class UserMapper {

	public Optional<UserDto> map(Optional<User> user) {
		UserDto userDto = new UserDto();
		if (user.isPresent()) {
			userDto.setId(user.get().getId());
			userDto.setFirstName(user.get().getFirstName());
			userDto.setLastName(user.get().getLastName());
			userDto.setEmail(user.get().getEmail());
			userDto.setPassword(user.get().getPassword());
			userDto.setLifeMotto(user.get().getLifeMotto());
		}
		return Optional.ofNullable(userDto);
	}

	public List<UserDto> map(List<User> users) {
		List<UserDto> list = new ArrayList<>();

		for (User element : users) {
			list.add(new UserDto(element.getId(), element.getFirstName(), element.getLastName(), element.getEmail(),
					element.getPassword(), element.getLifeMotto()));
		}
		return list;
	}
}
