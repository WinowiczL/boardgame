package com.winowicz.BoardGame.mapper.dto;

import java.util.List;

public class BoardGameDto {

	private Long id;
	private List<String> boardGamesNames;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<String> getBoardGamesNames() {
		return boardGamesNames;
	}

	public void setBoardGamesNames(List<String> boardGamesNames) {
		this.boardGamesNames = boardGamesNames;
	}

}
