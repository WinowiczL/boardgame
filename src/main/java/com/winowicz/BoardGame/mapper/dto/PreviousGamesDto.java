package com.winowicz.BoardGame.mapper.dto;

public class PreviousGamesDto {

	private Long id;

	private String previousGame;
	private String boardGameName;

	public PreviousGamesDto() {
	}

	public PreviousGamesDto(String previousGame, String boardGameName) {
		this.previousGame = previousGame;
		this.boardGameName = boardGameName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBoardGameName() {
		return boardGameName;
	}

	public void setBoardGameName(String boardGameName) {
		this.boardGameName = boardGameName;
	}

	public String getPreviousGame() {
		return previousGame;
	}

	public void setPreviousGame(String previousGame) {
		this.previousGame = previousGame;
	}

}
