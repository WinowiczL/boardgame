package com.winowicz.BoardGame.mapper.dto;

import java.util.List;

import com.winowicz.BoardGame.bo.AvailabilityPeriod;

public class PlayabilityDto {

	private List<AvailabilityPeriod> availabilityHours;
	private Long id;

	public PlayabilityDto() {

	}

	public PlayabilityDto(List<AvailabilityPeriod> availabilityHours) {
		this.availabilityHours = availabilityHours;
	}

	public List<AvailabilityPeriod> getAvailabilityHours() {
		return availabilityHours;
	}

	public void setAvailabilityHours(List<AvailabilityPeriod> availabilityHours) {
		this.availabilityHours = availabilityHours;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
