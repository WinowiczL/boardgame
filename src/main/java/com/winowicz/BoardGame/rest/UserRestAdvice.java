package com.winowicz.BoardGame.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.winowicz.BoardGame.exceptions.Error;
import com.winowicz.BoardGame.exceptions.UserDoesntExistException;

@ControllerAdvice
public class UserRestAdvice {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRestAdvice.class);

	@ResponseBody
	@ExceptionHandler(UserDoesntExistException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Error userExceptionHandler(Exception ex) {
		LOGGER.error("Error occured ", ex);
		return new Error(ex.getMessage());
	}

}
