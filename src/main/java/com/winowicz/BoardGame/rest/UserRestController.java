package com.winowicz.BoardGame.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.winowicz.BoardGame.bo.Criteria;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.mapper.dto.UserDto;
import com.winowicz.BoardGame.service.UserProfileService;

@RestController
public class UserRestController {

	@Autowired
	UserProfileService userProfileService;

	@GetMapping(value = "/find-user")
	public List<UserDto> findUser(@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("email") String email) {
		return userProfileService.findByNamesAndEmail(firstName, lastName, email);
	}

	@PostMapping(value = "/find-user-byCriteria")
	public List<UserDto> findUser(@RequestBody Criteria criteria) {
		return userProfileService.findByNamesAndEmail(criteria);
	}

	@PostMapping(value = "/add-user", consumes = "application/json", produces = "application/json")
	public void addUser(@RequestBody User user) {
		userProfileService.addUser(user);
	}

}
