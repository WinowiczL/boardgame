package com.winowicz.BoardGame.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winowicz.BoardGame.bo.AvailabilityPeriod;
import com.winowicz.BoardGame.dao.PlayabilityDao;
import com.winowicz.BoardGame.facade.PlayabilityFacade;
import com.winowicz.BoardGame.mapper.PlayabilityMapper;
import com.winowicz.BoardGame.mapper.dto.PlayabilityDto;
import com.winowicz.BoardGame.validator.PlayabilityValidator;

@Service
public class PlayabilityService implements PlayabilityFacade {

	private PlayabilityDao playabilityDao;
	private PlayabilityMapper mapper;
	private PlayabilityValidator validator;

	@Autowired
	public PlayabilityService(PlayabilityDao playabilityDao, PlayabilityMapper mapper, PlayabilityValidator validator) {
		this.playabilityDao = playabilityDao;
		this.mapper = mapper;
		this.validator = validator;
	}

	@Override
	public void addAvailabilityHours(Long id, AvailabilityPeriod availabilityHours) {
		if (validator.isAddingFutureAvailability(availabilityHours)
				&& validator.isStartTimeBeforeEnd(availabilityHours)) {
			playabilityDao.addAvailabilityHours(id, availabilityHours);
		}

	}

	@Override
	public void editAvailabilityHours(Long id, AvailabilityPeriod old, AvailabilityPeriod newone) {
		if (validator.isAvailabilityHoursInPlayable(old)) {
			playabilityDao.editAvailabilityHours(id, old, newone);
		}

	}

	@Override
	public String deleteAvailabilityHours(Long id, AvailabilityPeriod availabilityHours, String message) {
		if (validator.isAvailabilityHoursInPlayable(availabilityHours)) {
			playabilityDao.deleteAvailabilityHours(id, availabilityHours, message);
		}
		return message;

	}

	@Override
	public List<Long> checkUsersWithSimilarHours(Long id) {
		return playabilityDao.checkUsersWithSimilarHours(id);
	}

	@Override
	public Optional<PlayabilityDto> checkAvailabilityHoursOfUser(Long id) {
		return playabilityDao.checkAvailabilityHoursOfUser(id);
	}

}
