package com.winowicz.BoardGame.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winowicz.BoardGame.dao.BoardGameDao;
import com.winowicz.BoardGame.facade.BoardGamesCollectionFacade;
import com.winowicz.BoardGame.mapper.BoardGameMapper;
import com.winowicz.BoardGame.mapper.dto.BoardGameDto;
import com.winowicz.BoardGame.validator.BoardGameValidator;

@Service
public class BoardGamesCollectionService implements BoardGamesCollectionFacade {

	private BoardGameDao boardGameDao;
	private BoardGameMapper mapper;
	private BoardGameValidator validator;

	@Autowired
	public BoardGamesCollectionService(BoardGameDao boardGameDao, BoardGameMapper mapper,
			BoardGameValidator validator) {
		this.boardGameDao = boardGameDao;
		this.mapper = mapper;
		this.validator = validator;
	}

	@Override
	public Optional<BoardGameDto> checkGamesCollection(Long id) {
		return boardGameDao.checkGamesCollection(id);
	}

	@Override
	public void removeGameFromCollection(Long id, String boardGameName) {
		if (validator.isGameInCollectionExist(boardGameName,
				boardGameDao.checkGamesCollection(id).get().getBoardGamesNames())) {
			boardGameDao.removeGameFromCollection(id, boardGameName);
		}
	}

	@Override
	public void addNewGameToCollection(Long id, String boardGameName) {
		if (validator.isGameInSystemExist(boardGameName, boardGameDao.giveAllGameList())
				&& !validator.isDuplicateInCollection(boardGameName,
						boardGameDao.checkGamesCollection(id).get().getBoardGamesNames())) {
			boardGameDao.addNewGameToCollection(id, boardGameName);
		}
	}

	@Override
	public void addGameToSystemCollection(String boardGameName) {
		if (!validator.isDuplicateInSystem(boardGameName, boardGameDao.giveAllGameList())) {
			boardGameDao.addGameToSystem(boardGameName);
		}
	}

	@Override
	public List<String> checkAllGamesInSystem() {
		return boardGameDao.giveAllGameList();
	}

}
