package com.winowicz.BoardGame.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winowicz.BoardGame.bo.Criteria;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.dao.UserDao;
import com.winowicz.BoardGame.facade.UserProfileFacade;
import com.winowicz.BoardGame.mapper.UserMapper;
import com.winowicz.BoardGame.mapper.dto.UserDto;
import com.winowicz.BoardGame.validator.UserValidator;

@Service
public class UserProfileService implements UserProfileFacade {

	private UserDao userDao;
	private UserMapper mapper;
	private UserValidator validator;

	@Autowired
	public UserProfileService(UserDao userDao, UserMapper mapper, UserValidator userValidator) {
		this.userDao = userDao;
		this.mapper = mapper;
		this.validator = userValidator;
	}

	@Override
	public Optional<UserDto> checkProfileInformation(Long id) {
		return userDao.findById(id);
	}

	@Override
	public void editFirstNameProfile(Long id, String firstName) {
		if (validator.validate(firstName)) {
			userDao.editFirstNameProfile(id, firstName);
		}
	}

	@Override
	public void editLastNameProfile(Long id, String lastName) {
		if (validator.validate(lastName)) {
			userDao.editLastNameProfile(id, lastName);
		}
	}

	@Override
	public void editEmailProfile(Long id, String email) {
		if (validator.validate(email)) {
			userDao.editEmailProfile(id, email);
		}
	}

	@Override
	public void editPasswordProfile(Long id, String password) {
		if (validator.validate(password)) {
			userDao.editPasswordProfile(id, password);
		}

	}

	@Override
	public void editLifeMottoProfile(Long id, String lifeMotto) {
		if (validator.validate(lifeMotto)) {
			userDao.editLifeMottoProfile(id, lifeMotto);
		}

	}

	@Override
	public List<UserDto> findByNamesAndEmail(Criteria criteria) {
		List<UserDto> list = userDao.findByNamesAndEmail(criteria);
		if (validator.isAnyUser(list) && validator.isUserExist(criteria)) {
			return userDao.findByNamesAndEmail(criteria);
		}
		return list;
	}

	@Override
	public List<UserDto> findByNamesAndEmail(String firstName, String lastName, String email) {
		List<UserDto> list = userDao.findByNamesAndEmail(firstName, lastName, email);
		if (validator.isAnyUser(list) && validator.isUserExist(firstName, lastName, email)) {
			return userDao.findByNamesAndEmail(firstName, lastName, email);
		}
		return list;
	}

	@Override
	public void addUser(User user) {
		if (!validator.isUserEmailExist(user.getEmail(), listOfAllUsers()))
			userDao.addUser(user);
	}

	private List<User> listOfAllUsers() {
		return userDao.listOfAllUsers();
	}

}
