package com.winowicz.BoardGame.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winowicz.BoardGame.dao.PreviousGamesDao;
import com.winowicz.BoardGame.dao.StatisticDao;
import com.winowicz.BoardGame.facade.RankingInformationFacade;
import com.winowicz.BoardGame.mapper.dto.PreviousGamesDto;
import com.winowicz.BoardGame.mapper.dto.StatisticDto;
import com.winowicz.BoardGame.mapper.PreviousGamesMapper;
import com.winowicz.BoardGame.mapper.StatisticMapper;

@Service
public class RankingInformationService implements RankingInformationFacade {

	private StatisticDao statisticDao;
	private PreviousGamesDao previousGamesDao;
	private StatisticMapper statisticMapper;
	private PreviousGamesMapper previousGamesMapper;

	@Autowired
	public RankingInformationService(StatisticDao statisticDao, PreviousGamesDao previousGamesDao,
			StatisticMapper mapper, PreviousGamesMapper previousGamesMapper) {
		this.statisticDao = statisticDao;
		this.statisticMapper = statisticMapper;
		this.previousGamesDao = previousGamesDao;
		this.previousGamesMapper = previousGamesMapper;
	}

	@Override
	public Optional<StatisticDto> checkCurrentStatistics(Long userId) {
		return statisticDao.checkCurrentStatistics(userId);
	}

	@Override
	public Long checkCurrentLevel(Long userId) {
		return statisticDao.checkCurrentLevel(userId);
	}

	@Override
	public Long checkCurrentRankingPosition(Long userId) {
		return statisticDao.checkCurrentRankingPosition(userId);
	}

	@Override
	public List<PreviousGamesDto> checkHistoryOfPreviousGames(Long id) {
		return previousGamesDao.checkHistoryOfPreviousGames(id);
	}

}
