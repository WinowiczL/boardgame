package com.winowicz.BoardGame.dao;

import java.util.List;
import java.util.Optional;

import com.winowicz.BoardGame.mapper.dto.BoardGameDto;

public interface BoardGameDao {

	Optional<BoardGameDto> checkGamesCollection(Long id);

	void removeGameFromCollection(Long id, String gameName);

	void addNewGameToCollection(Long id, String gameName);

	void addGameToSystem(String boardGameName);

	List<String> giveAllGameList();
}
