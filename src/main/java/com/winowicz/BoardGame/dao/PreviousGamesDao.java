package com.winowicz.BoardGame.dao;

import java.util.List;

import com.winowicz.BoardGame.mapper.dto.PreviousGamesDto;

public interface PreviousGamesDao {

	public List<PreviousGamesDto> checkHistoryOfPreviousGames(Long id);

}
