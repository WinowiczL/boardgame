package com.winowicz.BoardGame.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.winowicz.BoardGame.bo.PreviousGames;
import com.winowicz.BoardGame.dao.PreviousGamesDao;
import com.winowicz.BoardGame.mapper.PreviousGamesMapper;
import com.winowicz.BoardGame.mapper.dto.PreviousGamesDto;

@Repository
public class PreviousGamesDaoImpl implements PreviousGamesDao {

	private PreviousGamesMapper previousGamesMapper;

	private static List<PreviousGames> games;

	@Autowired
	public PreviousGamesDaoImpl(PreviousGamesMapper previousGameMapper) {
		this.previousGamesMapper = previousGameMapper;
	}

	@Override
	public List<PreviousGamesDto> checkHistoryOfPreviousGames(Long id) {
		List<PreviousGames> list = findByIdOfUser(id);
		return previousGamesMapper.map(list);
	}

	private List<PreviousGames> findByIdOfUser(Long id) {
		return games.stream().filter(u -> u.getId().equals(id)).collect(Collectors.toCollection(ArrayList::new));
	}

	public static void resetPreviousGames() {

		games = new ArrayList<>();
		games.add(new PreviousGames("Fajna Gra", "monopoly"));
		games.add(new PreviousGames("Gra doszła do remisu", "monopoly"));
		games.add(new PreviousGames("Maciek wygrał jednym punktem", "monopoly"));
		games.add(new PreviousGames("Gracze spotkali się ale gra się nie odbyła", "monopoly"));
		games.add(new PreviousGames("Gracz został zbanowany za cheaty", "monopoly"));
	}

}
