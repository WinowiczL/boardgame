package com.winowicz.BoardGame.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.winowicz.BoardGame.bo.BoardGame;
import com.winowicz.BoardGame.dao.BoardGameDao;
import com.winowicz.BoardGame.mapper.BoardGameMapper;
import com.winowicz.BoardGame.mapper.dto.BoardGameDto;

@Repository
public class BoardGameDaoImpl implements BoardGameDao {

	private BoardGameMapper mapper;

	private static List<BoardGame> boardGames;

	@Autowired
	public BoardGameDaoImpl(BoardGameMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public Optional<BoardGameDto> checkGamesCollection(Long id) {
		Optional<BoardGame> boardGame = findByIdOfUser(id);
		return mapper.map(boardGame);
	}

	@Override
	public void removeGameFromCollection(Long id, String gameName) {
		Optional<BoardGame> boardGame = findByIdOfUser(id);
		if (boardGame.isPresent()) {
			boardGame.get().getBoardGamesNames().remove(gameName);
		}

	}

	@Override
	public void addNewGameToCollection(Long id, String gameName) {
		Optional<BoardGame> boardGame = findByIdOfUser(id);
		if (boardGame.isPresent()) {
			boardGame.get().getBoardGamesNames().add(gameName);
		}
	}

	@Override
	public void addGameToSystem(String gameBoardName) {
		gamesInSystem.add(gameBoardName);

	}

	@Override
	public List<String> giveAllGameList() {
		return gamesInSystem;
	}

	private Optional<BoardGame> findByIdOfUser(Long id) {
		return boardGames.stream().filter(b -> b.getId().equals(id)).findFirst();
	}

	public static void resetBoardGame() {
		boardGames = new ArrayList<>();
		boardGames.add(new BoardGame(new ArrayList<>(Arrays.asList("Monopoly", "Gra1"))));
		boardGames.add(new BoardGame(Arrays.asList("Gra2", "Gra3", "Gra4")));
		boardGames.add(new BoardGame(Arrays.asList("Gra5")));
		boardGames.add(new BoardGame(Arrays.asList("Gra6", "Gra7", "Monopoly")));
	}

	private List<String> gamesInSystem = new ArrayList<>(
			Arrays.asList("Monopoly", "Gra1", "Gra2", "Gra3", "Gra4", "Gra5", "Gra6", "Gra7"));

}
