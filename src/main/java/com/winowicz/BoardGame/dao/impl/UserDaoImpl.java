package com.winowicz.BoardGame.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.winowicz.BoardGame.bo.Criteria;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.dao.UserDao;
import com.winowicz.BoardGame.mapper.UserMapper;
import com.winowicz.BoardGame.mapper.dto.UserDto;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private UserMapper mapper;

	private static List<User> users;

	public UserDaoImpl(UserMapper mapper) {
		User.setNUM(0L);
		resetUsers();
		this.mapper = mapper;
	}

	@Override
	public Optional<UserDto> findById(Long id) {
		Optional<User> first = findUserById(id);
		return mapper.map(first);
	}

	private Optional<User> findUserById(Long id) {
		return users.stream().filter(u -> u.getId().equals(id)).findFirst();
	}

	@Override
	public void editFirstNameProfile(Long id, String firstName) {
		Optional<User> user = findUserById(id);
		if (user.isPresent()) {
			user.get().setFirstName(firstName);
		}
	}

	@Override
	public void editLastNameProfile(Long id, String lastName) {
		Optional<User> user = findUserById(id);
		if (user.isPresent()) {
			user.get().setLastName(lastName);
		}
	}

	@Override
	public void editEmailProfile(Long id, String email) {
		Optional<User> user = findUserById(id);
		if (user.isPresent()) {
			user.get().setEmail(email);
		}
	}

	@Override
	public void editPasswordProfile(Long id, String password) {
		Optional<User> user = findUserById(id);
		if (user.isPresent()) {
			user.get().setPassword(password);
		}
	}

	@Override
	public void editLifeMottoProfile(Long id, String lifeMotto) {
		Optional<User> user = findUserById(id);
		if (user.isPresent()) {
			user.get().setLifeMotto(lifeMotto);
		}
	}

	@Override
	public void addUser(User user) {
		users.add(user);
	}

	@Override
	public List<UserDto> findByNamesAndEmail(Criteria criteria) {

		Stream<User> mainStream = users.stream();

		if (firstNameIsNotEmpty(criteria)) {
			mainStream = mainStream.filter(u -> u.getFirstName().equalsIgnoreCase(criteria.getFirstName()));
		}

		if (lastNameIsNotEmpty(criteria)) {
			mainStream = mainStream.filter(u -> u.getLastName().equalsIgnoreCase(criteria.getLastName()));
		}

		if (emailIsNotEmpty(criteria)) {
			mainStream = mainStream.filter(u -> u.getEmail().equalsIgnoreCase(criteria.getEmail()));
		}

		ArrayList<User> list = mainStream.collect(Collectors.toCollection(ArrayList::new));

		return mapper.map(list);
	}

	@Override
	public List<UserDto> findByNamesAndEmail(String firstName, String lastName, String email) {
		List<User> list = new ArrayList<>();
		if (firstName != "" && lastName != "" && email != "") {
			list = findUserBy3Params(firstName, lastName, email);
		}

		if (firstName != "" && lastName != "" && email == "") {
			list = findUserByFirstNameLastName(firstName, lastName);
		}

		if (firstName != "" && lastName == "" && email != "") {
			list = findUserByFirstNameEmail(firstName, email);
		}

		if (firstName == "" && lastName != "" && email != "") {
			list = findUserByLastNameEmail(lastName, email);
		}

		if (firstName == "" && lastName == "" && email != "") {
			list = findUserByEmail(email);
		}

		if (firstName == "" && lastName != "" && email == "") {
			list = findUserByLastName(lastName);
		}

		if (firstName != "" && lastName == "" && email == "") {
			list = findUserByFirstName(firstName);
		}

		return mapper.map(list);

	}
	
	@Override
	public List<User> listOfAllUsers() {
		return users;
	}

	private List<User> findUserByFirstName(String firstName) {
		return users.stream().filter(u -> u.getFirstName().equals(firstName))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	private List<User> findUserByLastName(String lastName) {
		return users.stream().filter(u -> u.getLastName().equals(lastName))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	private List<User> findUserByEmail(String email) {
		return users.stream().filter(u -> u.getEmail().equals(email)).collect(Collectors.toCollection(ArrayList::new));
	}

	private List<User> findUserByLastNameEmail(String lastName, String email) {
		return users.stream().filter(u -> u.getLastName().equals(lastName) && u.getEmail().equals(email))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	private List<User> findUserByFirstNameEmail(String firstName, String email) {
		return users.stream().filter(u -> u.getFirstName().equals(firstName) && u.getEmail().equals(email))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	private List<User> findUserByFirstNameLastName(String firstName, String lastName) {
		return users.stream().filter(u -> u.getFirstName().equals(firstName) && u.getLastName().equals(lastName))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	private List<User> findUserBy3Params(String firstName, String lastName, String email) {
		return users.stream().filter(u -> u.getFirstName().equals(firstName) && u.getLastName().equals(lastName)
				&& u.getEmail().equals(email)).collect(Collectors.toCollection(ArrayList::new));
	}
	
	private boolean firstNameIsNotEmpty(Criteria criteria) {
		return criteria.getFirstName() != null && criteria.getFirstName() != "";
	}
	
	private boolean lastNameIsNotEmpty(Criteria criteria) {
		return criteria.getLastName() != null && criteria.getLastName() != "";
	}
	
	private boolean emailIsNotEmpty(Criteria criteria) {
		return criteria.getEmail() != null && criteria.getEmail() != "";
	}

	public static void resetUsers() {
		users = new ArrayList<>();
		users.add(new User("Adam", "Malysz", "haha@wp.pl", "haslo123", "motto"));
		users.add(new User("Tomasz", "Lis", "321@wp.pl", "haslomaslo", "lotto"));
		users.add(new User("Koko", "Dzambo", "encepence@wp.pl", "qwerty", "fotto"));
		users.add(new User("Kajko", "Kokosz", "emajl@wp.pl", "qweqwe", "a"));
		users.add(new User("Spider", "Man", "siec@wp.pl", "pajak", "pajakpajak"));
		users.add(new User("Tomasz", "Lis", "123@wp.pl", "haslomaslo", "lotto"));
	}

}
