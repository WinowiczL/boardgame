package com.winowicz.BoardGame.dao.impl;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.winowicz.BoardGame.bo.AvailabilityPeriod;
import com.winowicz.BoardGame.bo.Playability;
import com.winowicz.BoardGame.dao.PlayabilityDao;
import com.winowicz.BoardGame.mapper.PlayabilityMapper;
import com.winowicz.BoardGame.mapper.dto.PlayabilityDto;

@Repository
public class PlayabilityDaoImpl implements PlayabilityDao {

	@Autowired
	private PlayabilityMapper mapper;

	private static List<Playability> playabilitys;

	public PlayabilityDaoImpl(PlayabilityMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public void addAvailabilityHours(Long id, AvailabilityPeriod availabilityHours) {
		Optional<Playability> play = findUserById(id);
		play.get().getAvailabilityHours().add(availabilityHours);

	}

	@Override
	public void editAvailabilityHours(Long id, AvailabilityPeriod old, AvailabilityPeriod newone) {
		Optional<Playability> playability = findUserById(id);
		List<AvailabilityPeriod> list = playability.get().getAvailabilityHours();
		for (AvailabilityPeriod element : list) {
			if (element.equals(old)) {
				element.setStart(newone.getStart());
				element.setEnd(newone.getEnd());
			}
		}

	}

	@Override
	public String deleteAvailabilityHours(Long id, AvailabilityPeriod availabilityHours, String message) {
		Optional<Playability> playability = findUserById(id);
		List<AvailabilityPeriod> list = playability.get().getAvailabilityHours();
		for (AvailabilityPeriod element : list) {
			if (element.equals(availabilityHours)) {
				element.setStatus(false);
				element.setMessage(message);
			}
		}
		return message;
	}

	@Override
	public List<Long> checkUsersWithSimilarHours(Long id) {
		Optional<Playability> playability = findUserById(id);
		List<AvailabilityPeriod> list = playability.get().getAvailabilityHours();
		List<Long> opponents = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			for (Playability users : playabilitys) {
				if (users.getAvailabilityHours().contains(list.get(i))) {
					opponents.add(users.getId());
				}
			}
		}
		return opponents;
	}

	@Override
	public Optional<PlayabilityDto> checkAvailabilityHoursOfUser(Long id) {
		Optional<Playability> playability = findUserById(id);
		return mapper.map(playability);

	}

	private Optional<Playability> findUserById(Long id) {
		return playabilitys.stream().filter(u -> u.getId().equals(id)).findFirst();
	}

	public static void resetPlayability() {
		playabilitys = new ArrayList<>();
		AvailabilityPeriod availabilityHours1 = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 11, 12, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 15, 0), true, "");
		AvailabilityPeriod availabilityHours2 = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 11, 11, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 16, 0), true, "");
		AvailabilityPeriod availabilityHours3 = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 12, 12, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 15, 0), true, "");
		AvailabilityPeriod availabilityHours4 = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 11, 16, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 20, 0), true, "");
		AvailabilityPeriod availabilityHours5 = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 11, 12, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 15, 0), true, "");
		playabilitys.add(new Playability(new ArrayList<AvailabilityPeriod>(Arrays.asList(availabilityHours1))));
		playabilitys.add(new Playability(new ArrayList<AvailabilityPeriod>(Arrays.asList(availabilityHours2))));
		playabilitys.add(new Playability(new ArrayList<AvailabilityPeriod>(Arrays.asList(availabilityHours3))));
		playabilitys.add(new Playability(new ArrayList<AvailabilityPeriod>(Arrays.asList(availabilityHours4))));
		playabilitys.add(new Playability(new ArrayList<AvailabilityPeriod>(Arrays.asList(availabilityHours5))));

	}

}
