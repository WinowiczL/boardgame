package com.winowicz.BoardGame.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.winowicz.BoardGame.bo.Statistic;
import com.winowicz.BoardGame.dao.StatisticDao;
import com.winowicz.BoardGame.mapper.StatisticMapper;
import com.winowicz.BoardGame.mapper.dto.StatisticDto;

@Repository
public class StatisticDaoImpl implements StatisticDao {

	private StatisticMapper mapper;

	private static List<Statistic> statistics;

	@Autowired
	public StatisticDaoImpl(StatisticMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public Optional<StatisticDto> checkCurrentStatistics(Long id) {
		Optional<Statistic> statistic = findStatsById(id);
		return mapper.map(statistic);
	}

	@Override
	public Long checkCurrentLevel(Long id) {
		Optional<Statistic> statistic = findStatsById(id);
		return mapper.map(statistic).get().getLevel();
	}

	@Override
	public Long checkCurrentRankingPosition(Long id) {
		Optional<Statistic> statistic = findStatsById(id);
		return mapper.map(statistic).get().getRankingPosition();
	}

	private Optional<Statistic> findStatsById(Long id) {
		return statistics.stream().filter(s -> s.getId().equals(id)).findFirst();
	}

	public static void resetStatistics() {
		statistics = new ArrayList<>();
		statistics.add(new Statistic(5L, 3L, 0L, 15L, 3L));
		statistics.add(new Statistic(1L, 2L, 0L, 3L, 4L));
		statistics.add(new Statistic(1L, 1L, 1L, 1L, 1L));
		statistics.add(new Statistic(2L, 4L, 1L, 11L, 4L));
		statistics.add(new Statistic(3L, 5L, 1L, 1L, 8L));
	}

}
