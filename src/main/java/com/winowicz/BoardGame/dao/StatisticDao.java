package com.winowicz.BoardGame.dao;

import java.util.Optional;

import com.winowicz.BoardGame.mapper.dto.StatisticDto;

public interface StatisticDao {

	public Optional<StatisticDto> checkCurrentStatistics(Long id);

	public Long checkCurrentLevel(Long id);

	public Long checkCurrentRankingPosition(Long id);

}
