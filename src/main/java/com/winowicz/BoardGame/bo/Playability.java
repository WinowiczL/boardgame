package com.winowicz.BoardGame.bo;

import java.util.List;

public class Playability {

	private static Long num = 0L;

	private List<AvailabilityPeriod> availabilityHours;
	private Long id;

	public Playability() {
		this.id = num++;
	}

	public Playability(List<AvailabilityPeriod> availabilityHours) {
		this.id = num++;
		this.availabilityHours = availabilityHours;
	}

	public List<AvailabilityPeriod> getAvailabilityHours() {
		return availabilityHours;
	}

	public void setAvailabilityHours(List<AvailabilityPeriod> availabilityHours) {
		this.availabilityHours = availabilityHours;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static Long getNUM() {
		return num;
	}

	public static void setNUM(Long nUM) {
		num = nUM;
	}

}
