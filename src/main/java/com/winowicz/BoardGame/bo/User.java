package com.winowicz.BoardGame.bo;

public class User {

	private static Long num = 0L;

	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String lifeMotto;

	public User() {
		this.id = num++;
	}

	public User(String firstName, String lastName, String email, String password, String lifeMotto) {
		this.id = num++;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.lifeMotto = lifeMotto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLifeMotto() {
		return lifeMotto;
	}

	public void setLifeMotto(String lifeMotto) {
		this.lifeMotto = lifeMotto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static Long getNUM() {
		return num;
	}

	public static void setNUM(Long nUM) {
		num = nUM;
	}

}
