package com.winowicz.BoardGame.bo;

public class PreviousGames {

	private static Long num = 0L;

	private Long id;

	private String previousGame;
	private String boardGameName;

	public PreviousGames() {
		this.id = num++;
	}

	public PreviousGames(String previousGame, String boardGameName) {
		this.id = num++;
		this.previousGame = previousGame;
		this.boardGameName = boardGameName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBoardGameName() {
		return boardGameName;
	}

	public void setBoardGameName(String boardGameName) {
		this.boardGameName = boardGameName;
	}

	public String getPreviousGame() {
		return previousGame;
	}

	public void setPreviousGame(String previousGame) {
		this.previousGame = previousGame;
	}

	public static Long getNUM() {
		return num;
	}

	public static void setNUM(Long nUM) {
		num = nUM;
	}

}
