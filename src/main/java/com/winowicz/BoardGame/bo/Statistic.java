package com.winowicz.BoardGame.bo;

public class Statistic {

	private static Long num = 0L;

	private Long id;
	private Long win;
	private Long loss;
	private Long draw;
	private Long rankingPosition;
	private Long level;

	public Statistic() {
		this.id = num++;
	}

	public Statistic(Long win, Long loss, Long draw, Long rankingPosition, Long level) {
		this.id = num++;
		this.win = win;
		this.loss = loss;
		this.draw = draw;
		this.rankingPosition = rankingPosition;
		this.level = level;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getWin() {
		return win;
	}

	public void setWin(Long win) {
		this.win = win;
	}

	public Long getLoss() {
		return loss;
	}

	public void setLoss(Long loss) {
		this.loss = loss;
	}

	public Long getDraw() {
		return draw;
	}

	public void setDraw(Long draw) {
		this.draw = draw;
	}

	public Long getRankingPosition() {
		return rankingPosition;
	}

	public void setRankingPosition(Long rankingPosition) {
		this.rankingPosition = rankingPosition;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public static Long getNUM() {
		return num;
	}

	public static void setNUM(Long nUM) {
		num = nUM;
	}

}
