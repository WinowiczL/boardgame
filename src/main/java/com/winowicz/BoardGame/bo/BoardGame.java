package com.winowicz.BoardGame.bo;

import java.util.List;

public class BoardGame {

	private static Long num = 0L;

	private Long id;
	private List<String> boardGamesNames;

	public BoardGame() {
		this.id = num++;
	}

	public BoardGame(List<String> boardGamesNames) {
		this.id = num++;
		this.boardGamesNames = boardGamesNames;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<String> getBoardGamesNames() {
		return boardGamesNames;
	}

	public void setBoardGamesNames(List<String> boardGamesNames) {
		this.boardGamesNames = boardGamesNames;
	}

	public static Long getNUM() {
		return num;
	}

	public static void setNUM(Long nUM) {
		num = nUM;
	}

}
