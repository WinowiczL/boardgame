package com.winowicz.BoardGame.facade;

import java.util.List;
import java.util.Optional;

import com.winowicz.BoardGame.bo.AvailabilityPeriod;
import com.winowicz.BoardGame.mapper.dto.PlayabilityDto;

public interface PlayabilityFacade {

	void addAvailabilityHours(Long id, AvailabilityPeriod availabilityHours);

	void editAvailabilityHours(Long id, AvailabilityPeriod old, AvailabilityPeriod newone);

	String deleteAvailabilityHours(Long id, AvailabilityPeriod availabilityHours, String message);

	List<Long> checkUsersWithSimilarHours(Long id);

	Optional<PlayabilityDto> checkAvailabilityHoursOfUser(Long id);

}
