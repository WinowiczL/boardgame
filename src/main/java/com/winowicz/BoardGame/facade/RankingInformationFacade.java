package com.winowicz.BoardGame.facade;

import java.util.List;
import java.util.Optional;

import com.winowicz.BoardGame.mapper.dto.PreviousGamesDto;
import com.winowicz.BoardGame.mapper.dto.StatisticDto;

public interface RankingInformationFacade {

	public Optional<StatisticDto> checkCurrentStatistics(Long id);

	public Long checkCurrentLevel(Long id);

	public Long checkCurrentRankingPosition(Long id);

	public List<PreviousGamesDto> checkHistoryOfPreviousGames(Long id);

}
