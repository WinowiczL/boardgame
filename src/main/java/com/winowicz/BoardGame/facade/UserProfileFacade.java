package com.winowicz.BoardGame.facade;

import java.util.List;
import java.util.Optional;

import com.winowicz.BoardGame.bo.Criteria;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.mapper.dto.UserDto;

public interface UserProfileFacade {

	Optional<UserDto> checkProfileInformation(Long id);

	void editFirstNameProfile(Long id, String firstName);

	void editLastNameProfile(Long id, String lastName);

	void editEmailProfile(Long id, String email);

	void editPasswordProfile(Long id, String password);

	void editLifeMottoProfile(Long id, String lifeMotto);
	
	List<UserDto> findByNamesAndEmail (Criteria criteria);
	
	List<UserDto> findByNamesAndEmail (String firstName, String lastName, String email);
	
	void addUser(User user);

}
