package com.winowicz.BoardGame.facade;

import java.util.List;
import java.util.Optional;

import com.winowicz.BoardGame.mapper.dto.BoardGameDto;

public interface BoardGamesCollectionFacade {

	Optional<BoardGameDto> checkGamesCollection(Long id);

	void removeGameFromCollection(Long id, String boardGameName);

	void addNewGameToCollection(Long id, String boardGameName);

	void addGameToSystemCollection(String boardGameName);

	List<String> checkAllGamesInSystem();

}
