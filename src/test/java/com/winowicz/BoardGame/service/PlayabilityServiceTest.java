package com.winowicz.BoardGame.service;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;

import com.winowicz.BoardGame.bo.AvailabilityPeriod;
import com.winowicz.BoardGame.bo.Playability;
import com.winowicz.BoardGame.dao.PlayabilityDao;
import com.winowicz.BoardGame.dao.impl.PlayabilityDaoImpl;
import com.winowicz.BoardGame.exceptions.BadDataTimeException;
import com.winowicz.BoardGame.facade.PlayabilityFacade;
import com.winowicz.BoardGame.mapper.PlayabilityMapper;
import com.winowicz.BoardGame.validator.PlayabilityValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayabilityServiceTest {

	@Autowired
	private PlayabilityDao playabilityDao;

	@Autowired
	private PlayabilityMapper mapper;

	@Autowired
	private PlayabilityValidator validator;

	private PlayabilityFacade playabilityFacade;

	@Before
	public void setUp() {
		Playability.setNUM(0L);
		PlayabilityDaoImpl.resetPlayability();
	}

	@Test
	public void shouldAddAvailabiltyHours() {
		// given
		playabilityFacade = new PlayabilityService(playabilityDao, mapper, validator);
		AvailabilityPeriod availabilityHours = new AvailabilityPeriod(LocalDateTime.of(2018, Month.DECEMBER, 11, 11, 0),
				LocalDateTime.of(2018, Month.DECEMBER, 11, 16, 0), true, "");

		// when
		playabilityFacade.addAvailabilityHours(0L, availabilityHours);
		List<AvailabilityPeriod> list = playabilityFacade.checkAvailabilityHoursOfUser(0L).get().getAvailabilityHours();

		// then
		assertEquals(list.get(1), availabilityHours);
	}

	@Test(expected = BadDataTimeException.class)
	public void shouldNotAddAvailabiltyHours() {
		// given
		playabilityFacade = new PlayabilityService(playabilityDao, mapper, validator);
		AvailabilityPeriod availabilityHours = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 11, 11, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 16, 0), true, "");

		// when
		playabilityFacade.addAvailabilityHours(0L, availabilityHours);

		// then
		// exception
	}

	@Test(expected = BadDataTimeException.class)
	public void shouldNotAddWhenStartIsAfter() {
		// given
		playabilityFacade = new PlayabilityService(playabilityDao, mapper, validator);
		AvailabilityPeriod availabilityHours = new AvailabilityPeriod(LocalDateTime.of(2018, Month.DECEMBER, 12, 11, 0),
				LocalDateTime.of(2018, Month.DECEMBER, 11, 16, 0), true, "");

		// when
		playabilityFacade.addAvailabilityHours(0L, availabilityHours);

		// then
		// exception
	}

	@Test
	public void shouldDeleteAvailabiltyHours() {
		// given
		playabilityFacade = new PlayabilityService(playabilityDao, mapper, validator);
		AvailabilityPeriod availabilityHours = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 11, 11, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 16, 0), true, "");

		// when
		boolean result = true;
		playabilityFacade.deleteAvailabilityHours(1L, availabilityHours, "I dont have time");
		List<AvailabilityPeriod> list = playabilityFacade.checkAvailabilityHoursOfUser(1L).get().getAvailabilityHours();
		String message = list.get(0).getMessage();

		for (AvailabilityPeriod element : list) {
			if (element.equals(availabilityHours)) {
				result = false;
			}
		}
		// then
		assertTrue(result == false);
		assertEquals("I dont have time", message);
	}

	@Test(expected = BadDataTimeException.class)
	public void shouldNotDeleteAvailabiltyHours() {
		// given
		playabilityFacade = new PlayabilityService(playabilityDao, mapper, validator);
		AvailabilityPeriod availabilityHours = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 12, 12, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 15, 0), true, "");

		// when
		playabilityFacade.deleteAvailabilityHours(2L, availabilityHours, "I dont have time");
		List<AvailabilityPeriod> list = playabilityFacade.checkAvailabilityHoursOfUser(2L).get().getAvailabilityHours();
		playabilityFacade.deleteAvailabilityHours(2L, list.get(0), "For sure i delete 1 more time :)");

		// then
		// exception
	}

	@Test
	public void shouldEditAvailabiltyHours() {
		// given
		playabilityFacade = new PlayabilityService(playabilityDao, mapper, validator);
		AvailabilityPeriod old = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 11, 16, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 20, 0), true, "");
		AvailabilityPeriod newone = new AvailabilityPeriod(LocalDateTime.of(2018, Month.APRIL, 10, 16, 0),
				LocalDateTime.of(2018, Month.APRIL, 11, 20, 0), true, "");

		// when
		playabilityFacade.editAvailabilityHours(3L, old, newone);
		List<AvailabilityPeriod> list = playabilityFacade.checkAvailabilityHoursOfUser(3L).get().getAvailabilityHours();

		// then
		assertEquals(list.get(0), newone);
	}

}
