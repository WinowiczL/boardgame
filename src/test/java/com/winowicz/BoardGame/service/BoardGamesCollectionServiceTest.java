package com.winowicz.BoardGame.service;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.winowicz.BoardGame.bo.BoardGame;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.dao.BoardGameDao;
import com.winowicz.BoardGame.dao.impl.BoardGameDaoImpl;
import com.winowicz.BoardGame.exceptions.GameDoesntExistException;
import com.winowicz.BoardGame.exceptions.GameExistException;
import com.winowicz.BoardGame.facade.BoardGamesCollectionFacade;
import com.winowicz.BoardGame.mapper.BoardGameMapper;
import com.winowicz.BoardGame.mapper.dto.BoardGameDto;
import com.winowicz.BoardGame.validator.BoardGameValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BoardGamesCollectionServiceTest {

	@Autowired
	private BoardGameDao boardGameDao;

	@Autowired
	private BoardGameMapper mapper;

	@Autowired
	private BoardGameValidator validator;

	private BoardGamesCollectionFacade boardGamesCollectionFacade;

	// Games in system -> "Monopoly", "Gra1", "Gra2", "Gra3", "Gra4", "Gra5",
	// "Gra6", "Gra7"
	// Users games:
	// boardGames.add(new BoardGame(Arrays.asList("Monopoly", "Gra1")));
	// boardGames.add(new BoardGame(Arrays.asList("Gra2", "Gra3", "Gra4")));
	// boardGames.add(new BoardGame(Arrays.asList("Gra5")));
	// boardGames.add(new BoardGame(Arrays.asList("Gra6", "Gra7", "Monopoly")));

	@Before
	public void setUp() {
		BoardGame.setNUM(0L);
		BoardGameDaoImpl.resetBoardGame();
	}

	@Test
	public void shouldAddGameToUserCollection() {
		// given
		boardGamesCollectionFacade = new BoardGamesCollectionService(boardGameDao, mapper, validator);

		// when
		boardGamesCollectionFacade.addNewGameToCollection(0L, "Gra3");
		Optional<BoardGameDto> games = boardGamesCollectionFacade.checkGamesCollection(0L);
		String gameNr2 = games.get().getBoardGamesNames().get(2);

		// then
		assertEquals("Gra3", gameNr2);
	}

	@Test(expected = GameDoesntExistException.class)
	public void shouldNotAddGameToUserCollection() {
		// given
		boardGamesCollectionFacade = new BoardGamesCollectionService(boardGameDao, mapper, validator);

		// when
		boardGamesCollectionFacade.addNewGameToCollection(0L, "Makao");

		// then
		// exception
	}

	@Test
	public void shouldAddGameToSystemCollection() {
		// given
		boardGamesCollectionFacade = new BoardGamesCollectionService(boardGameDao, mapper, validator);

		// when
		boardGamesCollectionFacade.addGameToSystemCollection("Bierki");
		List<String> listOfGames = boardGamesCollectionFacade.checkAllGamesInSystem();

		// then
		assertTrue(listOfGames.contains("Bierki"));
	}

	@Test(expected = GameExistException.class)
	public void shouldNotAddGameDuplicate() {
		// given
		boardGamesCollectionFacade = new BoardGamesCollectionService(boardGameDao, mapper, validator);

		// when
		boardGamesCollectionFacade.addNewGameToCollection(0L, "Monopoly");

		// then
		// exception

	}

	@Test(expected = GameExistException.class)
	public void shouldNotAddToSystemDuplicate() {
		// given
		boardGamesCollectionFacade = new BoardGamesCollectionService(boardGameDao, mapper, validator);

		// when
		boardGamesCollectionFacade.addGameToSystemCollection("Gra1");

		// then
		// exception

	}

	@Test
	public void shouldRmoveGameFromCollection() {
		// given
		boardGamesCollectionFacade = new BoardGamesCollectionService(boardGameDao, mapper, validator);

		// when
		boardGamesCollectionFacade.removeGameFromCollection(0L, "Gra1");
		Optional<BoardGameDto> boardGameDto = boardGamesCollectionFacade.checkGamesCollection(1L);
		List<String> games = boardGameDto.get().getBoardGamesNames();

		// then
		assertFalse(games.contains("Gra1"));
	}

	@Test(expected = GameDoesntExistException.class)
	public void shouldNotRemove() {
		// given
		boardGamesCollectionFacade = new BoardGamesCollectionService(boardGameDao, mapper, validator);

		// when
		boardGamesCollectionFacade.removeGameFromCollection(0L, "Gra2");

		// then
		// exception

	}
}
