package com.winowicz.BoardGame.service;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.winowicz.BoardGame.bo.PreviousGames;
import com.winowicz.BoardGame.bo.Statistic;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.dao.UserDao;
import com.winowicz.BoardGame.dao.impl.PreviousGamesDaoImpl;
import com.winowicz.BoardGame.dao.impl.StatisticDaoImpl;
import com.winowicz.BoardGame.dao.impl.UserDaoImpl;
import com.winowicz.BoardGame.exceptions.BadValueOfInputException;
import com.winowicz.BoardGame.facade.UserProfileFacade;
import com.winowicz.BoardGame.mapper.UserMapper;
import com.winowicz.BoardGame.mapper.dto.UserDto;
import com.winowicz.BoardGame.service.UserProfileService;
import com.winowicz.BoardGame.validator.UserValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserProfileServiceTest {

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserMapper mapper;

	@Autowired
	private UserValidator validator;

	private UserProfileFacade userProfileFacade;

	@Before
	public void setUp() {
		User.setNUM(0L);
		UserDaoImpl.resetUsers();
	}

	@Test
	public void shouldGiveGoodFirstNameOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);
		User user = new User("Adam", "Malysz", "haha@wp.pl", "haslo123", "motto");

		// when
		Optional<UserDto> opUser = userProfileFacade.checkProfileInformation(0L);
		String firstName = opUser.get().getFirstName();

		// then
		assertEquals(user.getFirstName(), firstName);
	}

	@Test
	public void shouldGiveGoodLastNameOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);
		User user = new User("Adam", "Malysz", "haha@wp.pl", "haslo123", "motto");

		// when
		String lastName = userProfileFacade.checkProfileInformation(0L).get().getLastName();

		// then
		assertEquals(user.getLastName(), lastName);
	}

	@Test
	public void shouldGiveGoodEmailOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);
		User user = new User("Adam", "Malysz", "haha@wp.pl", "haslo123", "motto");

		// when
		String email = userProfileFacade.checkProfileInformation(0L).get().getEmail();

		// then
		assertEquals(user.getEmail(), email);
	}

	@Test
	public void shouldGiveGoodPasswordOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);
		User user = new User("Adam", "Malysz", "haha@wp.pl", "haslo123", "motto");

		// when
		String password = userProfileFacade.checkProfileInformation(0L).get().getPassword();

		// then
		assertEquals(user.getPassword(), password);
	}

	@Test
	public void shouldGiveGoodLifeMottoOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);
		User user = new User("Adam", "Malysz", "haha@wp.pl", "haslo123", "motto");

		// when
		String lifeMotto = userProfileFacade.checkProfileInformation(0L).get().getLifeMotto();

		// then
		assertEquals(user.getLifeMotto(), lifeMotto);
	}

	@Test(expected = BadValueOfInputException.class)
	public void shouldNotEditFirstName() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);

		// when
		userProfileFacade.editFirstNameProfile(1L, "");

		// then
		// exception expected

	}

	@Test
	public void shouldEditFirstNameOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);

		// when
		userProfileFacade.editFirstNameProfile(2L, "Krystian");

		// then
		assertEquals("Krystian", userProfileFacade.checkProfileInformation(2L).get().getFirstName());
	}

	@Test
	public void shouldEditLastNameOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);

		// when
		userProfileFacade.editLastNameProfile(2L, "Mazurek");

		// then
		assertEquals("Mazurek", userProfileFacade.checkProfileInformation(2L).get().getLastName());
	}

	@Test
	public void shouldEditEmailOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);

		// when
		userProfileFacade.editEmailProfile(2L, "nowyemajl@wp.pl");

		// then
		assertEquals("nowyemajl@wp.pl", userProfileFacade.checkProfileInformation(2L).get().getEmail());
	}

	@Test
	public void shouldEditPasswordOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);

		// when
		userProfileFacade.editPasswordProfile(2L, "qwerty123");

		// then
		assertEquals("qwerty123", userProfileFacade.checkProfileInformation(2L).get().getPassword());
	}

	@Test
	public void shouldEditLifeMottoOfUser() {
		// given
		userProfileFacade = new UserProfileService(userDao, mapper, validator);

		// when
		userProfileFacade.editLifeMottoProfile(2L, "cymcyrymcy");

		// then
		assertEquals("cymcyrymcy", userProfileFacade.checkProfileInformation(2L).get().getLifeMotto());
	}
}
