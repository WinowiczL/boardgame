package com.winowicz.BoardGame.service;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.winowicz.BoardGame.bo.Playability;
import com.winowicz.BoardGame.bo.PreviousGames;
import com.winowicz.BoardGame.bo.Statistic;
import com.winowicz.BoardGame.dao.PreviousGamesDao;
import com.winowicz.BoardGame.dao.StatisticDao;
import com.winowicz.BoardGame.dao.impl.PlayabilityDaoImpl;
import com.winowicz.BoardGame.dao.impl.PreviousGamesDaoImpl;
import com.winowicz.BoardGame.dao.impl.StatisticDaoImpl;
import com.winowicz.BoardGame.facade.RankingInformationFacade;
import com.winowicz.BoardGame.mapper.PreviousGamesMapper;
import com.winowicz.BoardGame.mapper.StatisticMapper;
import com.winowicz.BoardGame.mapper.dto.PreviousGamesDto;
import com.winowicz.BoardGame.mapper.dto.StatisticDto;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RankingInformationServiceTest {

	@Autowired
	private StatisticDao statisticDao;
	
	@Autowired
	private PreviousGamesDao previousGamesDao;

	@Autowired
	private StatisticMapper statisticMapper;

	@Autowired
	private PreviousGamesMapper previousGamesMapper;

	private RankingInformationFacade rankingInformationFacade;
	
	@Before
	public void setUp() {
		Statistic.setNUM(0L);
		PreviousGames.setNUM(0L);
		StatisticDaoImpl.resetStatistics();
		PreviousGamesDaoImpl.resetPreviousGames();
	}

	@Test
	public void shouldGiveGoodLevel() {
		// given
		rankingInformationFacade = new RankingInformationService(statisticDao, previousGamesDao, statisticMapper, previousGamesMapper);
		Long levelOfUserWithIdZero = 3L;

		// when
		Long level = rankingInformationFacade.checkCurrentLevel(0L);

		// then
		assertEquals(levelOfUserWithIdZero, level);
	}
	
	@Test
	public void shouldGiveGoodRankingPosition() {
		// given
		rankingInformationFacade = new RankingInformationService(statisticDao, previousGamesDao, statisticMapper, previousGamesMapper);
		Long levelOfUserWithIdFour = 1L;

		// when
		Long level = rankingInformationFacade.checkCurrentRankingPosition(4L);

		// then
		assertEquals(levelOfUserWithIdFour, level);
	}
	
	@Test
	public void shouldGiveGoodStatistics() {
		// given
		rankingInformationFacade = new RankingInformationService(statisticDao, previousGamesDao, statisticMapper, previousGamesMapper);
		Long winOfUserZero = 5L;

		// when
		Optional<StatisticDto> stats = rankingInformationFacade.checkCurrentStatistics(0L);
		Long win = stats.get().getWin();

		// then
		assertEquals(winOfUserZero, win);
	}
	
	@Test
	public void shouldGiveGoodPreviousGames() {
		// given
		rankingInformationFacade = new RankingInformationService(statisticDao, previousGamesDao, statisticMapper, previousGamesMapper);
		String previousGames = new PreviousGames("Fajna Gra", "monopoly").getPreviousGame();

		// when
		List<PreviousGamesDto> list = rankingInformationFacade.checkHistoryOfPreviousGames(0L);
		String fromList = list.get(0).getPreviousGame();

		// then
		assertEquals(previousGames, fromList);
	}
}
