package com.winowicz.BoardGame.rest;

import static org.junit.Assert.*;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.winowicz.BoardGame.bo.Criteria;
import com.winowicz.BoardGame.bo.User;
import com.winowicz.BoardGame.dao.impl.UserDaoImpl;
import com.winowicz.BoardGame.exceptions.UserDoesntExistException;
import com.winowicz.BoardGame.exceptions.UserExistException;
import com.winowicz.BoardGame.mapper.dto.UserDto;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRestControllerTest {

	@Autowired
	private UserRestController userRestController;

	@Before
	public void setUp() {
		User.setNUM(0L);
		UserDaoImpl.resetUsers();
	}

	@Test
	public void shouldAddUser() {
		// given
		User user = new User("Michał", "Tomek", "uf@wp.pl", "haslo123", "motto");

		// when
		userRestController.addUser(user);
		List<UserDto> list = userRestController.findUser("Michał", "Tomek", "uf@wp.pl");

		// then
		assertTrue(list.size() > 0);
	}

	@Test(expected = UserExistException.class)
	public void shouldNotAddUser() {
		// given
		User user = new User("Michał", "Tomek", "uf@wp.pl", "haslo123", "motto");

		// when
		userRestController.addUser(user);
		userRestController.addUser(user);

		// then
		// exception
	}

	@Test
	public void shouldFindUser() {
		// given
		User user = new User("Michał", "Tomek", "uf@wp.pl", "haslo123", "motto");
		Criteria criteria = new Criteria("Michał", "Tomek", null);

		// when
		userRestController.addUser(user);
		List<UserDto> list = userRestController.findUser(criteria);

		// then
		assertTrue(list.size() > 0);
	}

	@Test(expected = UserDoesntExistException.class)
	public void shouldNotFindUser() {
		// given
		Criteria criteria = new Criteria(null, null, null);

		// when
		userRestController.findUser(criteria);

		// then
		// exception
	}

}
